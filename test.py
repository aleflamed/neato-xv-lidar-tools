from os import system
import ctypes
import lidar2store
import pandas as pd


def one(N, p0, p1):
    system('sed -i -e "s/const unsigned bytesToCheck = .*;/const unsigned bytesToCheck = {};/" checksum.c'.format(N))
    system('make >/dev/null')
    packet = ctypes.c_uint16 * N
    packet = packet()
    packet[0] = p0
    packet[1] = p1
    lib = ctypes.CDLL('./libchecksum.so')
    assert lib.get_uint16_t_size() == 2
    print("======== libc ===========")
    libc = lib.checksum(packet)
    print("======== py3  ===========")
    pyc = lidar2store.checksum(packet)
    return (N, libc, pyc)


def test():
    dfs = []
    # 0,0 - same, not interesting ; rest the same
    #for p0, p1 in [(0, 1)]:
    for p0, p1 in [(1, 0), (0, 1), (255, 0), (0, 255)]:
        # range(2, 22, 2)
        # 14: 6  14        1  8192   1   0
        df = pd.DataFrame([one(N, p0=p0, p1=p1) for N in [14]], columns=['N', 'arduino', 'py3'])
        df['p0'] = p0
        df['p1'] = p1
        dfs.append(df)
    df = pd.concat(dfs)
    print(df)


if __name__ == '__main__':
    test()
