#include <stdio.h>

typedef unsigned char byte;
typedef unsigned short uint16_t;

const unsigned bytesToCheck = 14; // 20  // 22 = packet length, last 2 are checksum

int get_uint16_t_size(void)
{
    return sizeof(uint16_t);
}

unsigned long checksum(uint16_t* Packet) {
  unsigned long chk32;
  unsigned long checksum;
  const int CalcCRC_Len = bytesToCheck / 2;
  uint16_t CalcCRC[CalcCRC_Len];

  byte b1a, b1b, b2a, b2b;
  int ix;

  for (int ix = 0; ix < CalcCRC_Len; ix++)       // initialize 'CalcCRC' array
    CalcCRC[ix] = 0;

  // Perform checksum validity test
  for (ix = 0; ix < bytesToCheck; ix += 2) {     // build 'CalcCRC' array
      CalcCRC[ix / 2] = Packet[ix] + ((Packet[ix + 1]) << 8);
  }

  chk32 = 0;
  for (ix = 0; ix < CalcCRC_Len; ix++) {
      unsigned long pre = chk32;
    chk32 = (chk32 << 1) + CalcCRC[ix];
    printf("%d, %d, %d, %d\n", ix, CalcCRC[ix], pre, chk32);
  }
  checksum = (chk32 & 0x7FFF) + (chk32 >> 15);
  checksum &= 0x7FFF;
  return checksum;
}

